package ru.amster.tm.constant;

public class ProgramArgConst {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    public static final String INFO = "-i";

}