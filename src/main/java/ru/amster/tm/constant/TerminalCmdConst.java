package ru.amster.tm.constant;

public class TerminalCmdConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CLEAR = "task-clr";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_UPDATE_BY_INDEX = "task-upd-i";

    public static final String TASK_UPDATE_BY_ID = "task-upd-id";

    public static final String TASK_VIEW_BY_ID = "task-v-id";

    public static final String TASK_VIEW_BY_NAME = "task-v-name";

    public static final String TASK_VIEW_BY_INDEX = "task-v-i";

    public static final String TASK_REMOVE_BY_ID = "task-re-id";

    public static final String TASK_REMOVE_BY_NAME = "task-re-name";

    public static final String TASK_REMOVE_BY_INDEX = "task-re-i";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-upd-i";

    public static final String PROJECT_UPDATE_BY_ID = "project-upd-id";

    public static final String PROJECT_VIEW_BY_ID = "project-v-id";

    public static final String PROJECT_VIEW_BY_NAME = "project-v-name";

    public static final String PROJECT_VIEW_BY_INDEX = "project-v-i";

    public static final String PROJECT_REMOVE_BY_ID = "project-re-id";

    public static final String PROJECT_REMOVE_BY_NAME = "project-re-name";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-re-i";

}