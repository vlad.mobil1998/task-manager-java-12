package ru.amster.tm.api.repository;

import ru.amster.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneById(String id);

}