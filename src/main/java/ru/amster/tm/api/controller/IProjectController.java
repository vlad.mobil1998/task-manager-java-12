package ru.amster.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProject();

    void createProject();

    void showProjectById();

    void showProjectByName();

    void showProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

    void removeProjectById();

}